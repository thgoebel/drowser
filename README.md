# Drowser

Avoid rogue apps from running in the background with Drowser.

Drowser is a simple app that kills the apps you select when the screen turns off.

It requires root and uses it to kill the apps with an `am force-stop <app-id>`. This is a *very* impolite way of stopping apps. These might (and will probably) misbehave. You might also lose data because of this. **Beware!**

## Download it

[<img src="https://f-droid.org/badge/get-it-on.png"
      alt="Get it on F-Droid"
      height="80">](https://f-droid.org/app/com.jarsilio.android.drowser)
